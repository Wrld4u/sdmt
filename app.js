const express = require('express')
const config = require('config')
const path = require('path')
const fileUpload = require('express-fileupload')
const cors = require('cors')
const app = express()

app.use(express.json({ extended: true }))
app.use(fileUpload({ createParentPath: true }))



// Альтернативно, разрешение всех источников (не рекомендуется для production)
app.use(cors())


app.use('/api/auth', require('./routes/auth.routes'))
app.use('/api/project', require('./routes/project.routes'))
app.use('/api/search', require('./routes/search.routes'))
app.use('/api/jira', require('./routes/jira.routes'))
app.use('/api/block', require('./routes/block.routes'))
app.use('/api/skill', require('./routes/skill.routes'))
app.use('/api/skillAndPeriod', require('./routes/SkillAndPeriod.routes'))
app.use('/api/team', require('./routes/team.routes'))
app.use('/api/roadmap', require('./routes/roadmap.routes'))

if (process.env.NODE_ENV === 'production') {
    app.use('/', express.static(path.join(__dirname, 'client', 'build')))

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
    })
}

const db = require("./models")
db.sequelize.sync()

// working test
app.get('/', async (req, res) => {
    res.status(200).json({ message: `working good! :)` })
})

const PORT = process.env.PORT || config.get('port') || 5000

async function start() {
    try {
        app.listen(PORT, () => {
            console.log(`Server has been started on ${PORT}`)
        })
    } catch (e) {
        console.log(`Server error: ${e.message}`)
        process.exit(1)
    }
}

start()