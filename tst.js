const fs = require('fs').promises;
const path = require('path');

async function createFolder() {
    let tmpDir = String(+new Date());
    let relPath = path.join(__dirname, 'client', 'public', 'assets', 'zips', tmpDir);

    try {
        await fs.mkdir(relPath, { recursive: true });
        console.log(':', relPath);
    } catch (e) {
        console.error('Ошибка при создании папки:', e.message);
    }
}

createFolder();