const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const fetch = require('node-fetch')
const Project = db.project

const Op = db.Sequelize.Op

const router = Router()


const getJira = (host, username, password) => {
    return new JiraApi({
        protocol: 'https',
        host,
        username,
        password, //TOKEN
        apiVersion: '3',
        strictSSL: true
    })
}

const request = async (url, login, pass, method = 'GET', body = null) => {
    try {
        let str = login+':'+pass

        const headers = {
            'Authorization': `Basic ${Buffer.from(
                str
            ).toString('base64')}`,
            'Accept': 'application/json'
        }

        const response = await fetch(url, {
            method,
            headers
        })

        return JSON.parse(await response.text())

    } catch (e) {
        console.log(e)
        throw e
    }
}


// route prefix = /api/search

// get all releases
router.post('/', auth, async (req, res) => {
    try {
        const { searchStr } = req.body

        console.log('searchStr', searchStr)
        const prjs = await Project.findAll()

        let projectsNamed = []
        let projectsWithIssues = []

        if (prjs.length) {
            projectsNamed = prjs.filter(p => p.name.toLowerCase().includes(searchStr.toLowerCase() || p.description.toLowerCase().includes(searchStr.toLowerCase())))

            for (let i = 0; i < prjs.length; i++) {
                if (prjs[i].jiraUrl && prjs[i].jiraName && prjs[i].jiraPass) {
                    // All issues of project
                    const els = await request(`https://${prjs[i].jiraUrl}/rest/api/3/search?jql=project=${prjs[i].jiraProject}&maxResults=-1`, prjs[i].jiraName, prjs[i].jiraPass)
                    // if (els.issues.filter(ss => (ss.fields.issuetype.name === 'Эпик' || ss.fields.issuetype.name === 'Задача') && ss.fields.summary.toLowerCase().includes(searchStr.toLowerCase())).length > 0) projectsWithIssues.push(prjs[i])
                    if (els.issues.filter(ss => ss.fields.issuetype.name === 'Эпик' && ss.fields.summary.toLowerCase().includes(searchStr.toLowerCase())).length > 0) projectsWithIssues.push(prjs[i])
                }
            }
        }

        res.json({ projectsNamed, projectsWithIssues, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})


module.exports = router