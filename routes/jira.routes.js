const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const fetch = require('node-fetch')
const db = require("../models")
const JiraApi = require('jira-client')

// const User = db.user

const Op = db.Sequelize.Op
// ATATT3xFfGF0I0WuZPAHtvVRJDSHZ5fw0yY89Ya1JTtiaAL5YIIzMDJLdXnOy4rB2UoQFaU4s7XiXmlwGqI2xP5Q-ACr13bzDGamxk8XIu4_DL6phPMDzkdGE5ZzH5YclBD2pecpS96rMPV8pYyglhHupr6BbUwTCDNZEjy7WPfcB_AgkKKRD0Q=A101FA1A
const router = Router()

// route prefix = /api/jira

const getJira = (host, username, password) => {
    return new JiraApi({
        protocol: 'https',
        host,
        username,
        password, //TOKEN
        apiVersion: '3',
        strictSSL: true
    })
}

const request = async (url, login, pass, method = 'GET', body = null) => {
    try {
        let str = login+':'+pass

        const headers = {
            'Authorization': `Basic ${Buffer.from(
                str
            ).toString('base64')}`,
                'Accept': 'application/json'
        }

        const response = await fetch(url, {
            method,
            headers
        })

        return JSON.parse(await response.text())

    } catch (e) {
        console.log(e)
        throw e
    }
}

// const jira = new JiraApi({
//     protocol: 'https',
//     host: 'slcty.atlassian.net',
//     username: 'wrld4work@gmail.com',
//     password: 'rVKkVktwiLH5jkOyw9ykC6B7', //TOKEN
//     apiVersion: '2',
//     strictSSL: true
// })

router.get('/', auth, async (req, res) => {
    try {
        const jira = getJira('slcty.atlassian.net', 'wrld4work@gmail.com', 'rVKkVktwiLH5jkOyw9ykC6B7')

        // let boards = await jira.getAllBoards()
        // let boards = await jira.searchProjects('TS')
        const projects = await jira.listProjects()

        // console.log(boards)
        // console.log('Projects', await jira.getProjects(boards.values[0].id))
        console.log('Projects', projects)

        res.json({ connected: true, message: 'ok' })
    } catch (e) {
        res.json({connected: false, message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

router.post('/boards', auth, async (req, res) => {
    try {
        const {  jiraUrl, jiraName, jiraPass } = req.body
        const jira = getJira(jiraUrl, jiraName, jiraPass)

        let boards = await jira.getAllBoards()

        // console.log(boards)
        // console.log('Projects', await jira.getProjects(boards.values[0].id))

        res.json({ connected: true, boards: boards.values, message: 'Успешно!' })
    } catch (e) {
        res.json({connected: false, message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

router.post('/projects', auth, async (req, res) => {
    try {
        const {  jiraUrl, jiraName, jiraPass, jiraProjectBoard, jiraHRProjectBoard, hr } = req.body

        const jira = getJira(jiraUrl, jiraName, jiraPass)

        let prj = null
        if (!hr) {
            prj = await jira.getProjects(jiraProjectBoard)
        } else {
            prj = await jira.getProjects(jiraHRProjectBoard)
        }

        res.json({ connected: true, projects: prj.values, message: 'Успешно!' })
    } catch (e) {
        res.json({connected: false, message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

router.post('/allprojects', auth, async (req, res) => {
    try {
        const {  jiraUrl, jiraName, jiraPass } = req.body

        const jira = getJira(jiraUrl, jiraName, jiraPass)

        const projects = await jira.listProjects()

        res.json({ connected: true, projects: projects, message: 'Успешно!' })
    } catch (e) {
        res.json({connected: false, message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

router.post('/allskills', auth, async (req, res) => {
    try {
        const {  jiraUrl, jiraName, jiraPass, prjName } = req.body

        const lbl = await request(`https://${jiraUrl}/rest/api/3/label`, jiraName, jiraPass)
        // console.log('lbl', lbl.values)

        res.json({ connected: true, labels: lbl.values, message: 'Успешно!' })
    } catch (e) {
        // console.log(`Что-то пошло не так! Ошибка: ${e.message}`)
        res.json({connected: false, message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

router.post('/allUsers', auth, async (req, res) => {
    try {
        const {  jiraUrl, jiraName, jiraPass } = req.body

        const jira = getJira(jiraUrl, jiraName, jiraPass)

        let users = await jira.getUsers()
        users = users.filter(u => u.hasOwnProperty('locale'))

        // let statuses = await jira.listStatus()

        res.json({ connected: true, users, message: 'Успешно!' })
    } catch (e) {
        // console.log(`Что-то пошло не так! Ошибка: ${e.message}`)
        res.json({connected: false, message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

router.post('/roadmap', auth, async (req, res) => {
    try {
        const {  jiraUrl, jiraName, jiraPass, prjName, HRprjName } = req.body

        const jira = getJira(jiraUrl, jiraName, jiraPass)

        // project
        let prj = await jira.getProject(prjName)

        // const prjStats = await request(`https://${jiraUrl}/rest/api/3/project/${prj.key}/statuses`, jiraName, jiraPass)
        // console.log('=========================')
        // console.log(prjStats)
        // // console.log(prjStats[0].statuses)
        // console.log('=========================')

        let statuses = await jira.listStatus()
        statuses = statuses.filter(st => st.scope && st.scope.project.id === prj.id).map(st => {return {id: st.id, name: st.name}})

        // All issues of project
        const els = await request(`https://${jiraUrl}/rest/api/3/search?jql=project=${prjName}&maxResults=-1`, jiraName, jiraPass)

        // All issues of HRproject
        const HRels = await request(`https://${jiraUrl}/rest/api/3/search?jql=project=${HRprjName}&maxResults=-1`, jiraName, jiraPass)

        // els.issues.forEach(ss => console.log(ss.fields.issuetype.name))

        // В поле fields оригинальное содержимое, всё с поля summary извлечено из поля fields
        let epics = els.issues.filter(ss => ss.fields.issuetype.name === 'Эпик').map(ss => {
            return {
                id: ss.id,
                key: ss.key,
                fields: ss.fields,
                summary: ss.fields.summary,
                status: {id: ss.fields.status.id, name: ss.fields.status.name},
                labels: ss.fields.labels,
                // parent: {id: ss.fields.parent.id, key: ss.fields.parent.key, fields: ss.fields.parent.fields},
                progress: ss.fields.progress,
                aggregateprogress: ss.fields.aggregateprogress,
                priority: {id: ss.fields.priority.id, name: ss.fields.priority.name},
                duedate: ss.fields.duedate,
                subtasks: ss.fields.subtasks,
                creator: {accountId: ss.fields.creator.accountId, accountType: ss.fields.creator.accountType, active: ss.fields.creator.active, displayName: ss.fields.creator.displayName, emailAddress: ss.fields.creator.emailAddress},
                description: ss.fields.description,
                timeestimate: ss.fields.timeestimate,
                aggregatetimeestimate: ss.fields.aggregatetimeestimate,
                timeoriginalestimate: ss.fields.timeoriginalestimate,
                aggregatetimeoriginalestimate: ss.fields.aggregatetimeoriginalestimate,
                resolutiondate: ss.fields.resolutiondate,
                resolution: ss.fields.resolution,
                timespent: ss.fields.timespent,
                aggregatetimespent: ss.fields.aggregatetimespent,
                hrTasks: [...HRels.issues.filter(hr => hr.fields.labels[0] === ss.id).map(hr => {
                    return {
                        id: hr.id,
                        key: hr.key,
                        summary: hr.fields.summary,
                        status: {id: hr.fields.status.id, name: hr.fields.status.name},
                        description: hr.fields.description,
                    }
                })],
                tasks: [...els.issues.filter(t => t.fields.issuetype.name === 'Задача' && t.fields.parent.id === ss.id).map(t => {
                    return {
                        id: t.id,
                        key: t.key,
                        fields: t.fields,
                        summary: t.fields.summary,
                        status: {id: t.fields.status.id, name: t.fields.status.name},
                        labels: t.fields.labels,
                        parent: {id: t.fields.parent.id, key: t.fields.parent.key, fields: t.fields.parent.fields},
                        progress: t.fields.progress,
                        aggregateprogress: t.fields.aggregateprogress,
                        priority: {id: t.fields.priority.id, name: t.fields.priority.name},
                        duedate: t.fields.duedate,
                        subtasks: t.fields.subtasks,
                        creator: {accountId: t.fields.creator.accountId, accountType: t.fields.creator.accountType, active: t.fields.creator.active, displayName: t.fields.creator.displayName, emailAddress: t.fields.creator.emailAddress},
                        description: t.fields.description,
                        timeestimate: t.fields.timeestimate,
                        aggregatetimeestimate: t.fields.aggregatetimeestimate,
                        timeoriginalestimate: t.fields.timeoriginalestimate,
                        aggregatetimeoriginalestimate: t.fields.aggregatetimeoriginalestimate,
                        resolutiondate: t.fields.resolutiondate,
                        resolution: t.fields.resolution,
                        timespent: t.fields.timespent,
                        aggregatetimespent: t.fields.aggregatetimespent,
                    }
                })],
               }
        })

        // В поле fields оригинальное содержимое, всё с поля summary извлечено из поля fields. Упаковано в epics
        // let tasks = els.issues.filter(ss => ss.fields.issuetype.name === 'Задача').map(ss => {
        //     return {
        //         id: ss.id,
        //         key: ss.key,
        //         fields: ss.fields,
        //         summary: ss.fields.summary,
        //         status: {id: ss.fields.status.id, name: ss.fields.status.name},
        //         labels: ss.fields.labels,
        //         parent: {id: ss.fields.parent.id, key: ss.fields.parent.key, fields: ss.fields.parent.fields},
        //         progress: ss.fields.progress,
        //         aggregateprogress: ss.fields.aggregateprogress,
        //         priority: {id: ss.fields.priority.id, name: ss.fields.priority.name},
        //         duedate: ss.fields.duedate,
        //         subtasks: ss.fields.subtasks,
        //         creator: {accountId: ss.fields.creator.accountId, accountType: ss.fields.creator.accountType, active: ss.fields.creator.active, displayName: ss.fields.creator.displayName, emailAddress: ss.fields.creator.emailAddress},
        //         description: ss.fields.description,
        //         timeestimate: ss.fields.timeestimate,
        //         aggregatetimeestimate: ss.fields.aggregatetimeestimate,
        //         timeoriginalestimate: ss.fields.timeoriginalestimate,
        //         aggregatetimeoriginalestimate: ss.fields.aggregatetimeoriginalestimate,
        //         resolutiondate: ss.fields.resolutiondate,
        //         resolution: ss.fields.resolution,
        //         timespent: ss.fields.timespent,
        //         aggregatetimespent: ss.fields.aggregatetimespent,
        //     }
        // })

        res.json({ connected: true, statuses, epics, message: 'Success!' })
    } catch (e) {
        // console.log(`Что-то пошло не так! Ошибка: ${e.message}`)
        res.json({connected: false, message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// Create Epic
router.post('/createEpics', auth, async (req, res) => {
    try {
        const {  jiraUrl, jiraName, jiraPass, prjName, epics } = req.body

        const jira = getJira(jiraUrl, jiraName, jiraPass)

        // project
        let prj = await jira.getProject(prjName)

        let epicId = prj.issueTypes.find(el => el.name === 'Эпик').id

        if (prj && epics.length && epicId) {
            for (let i=0; i<epics.length; i++) {
                await jira.addNewIssue({
                    fields: {
                        summary: epics[i].name,
                        description: {
                            type: 'doc',
                            version: 1,
                            content: [
                                {
                                    type: 'paragraph',
                                    content: [
                                        {
                                            text: epics[i].description,
                                            type: 'text'
                                        }
                                    ]
                                }
                            ]
                        },
                        issuetype: {
                            // id: '10002',
                            id: epicId,
                            name: 'Эпик'
                        },
                        project: {
                            id: prj.id
                        }
                    },
                })
            }
        }

        res.json({ connected: true, message: 'Успешно!' })
    } catch (e) {
        // console.log(`Что-то пошло не так! Ошибка: ${e.message}`)
        res.json({connected: false, message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// Create Task
router.post('/createTask', auth, async (req, res) => {
    try {
        const {  jiraUrl, jiraName, jiraPass, prjName, task } = req.body

        const jira = getJira(jiraUrl, jiraName, jiraPass)

        // project
        let prj = await jira.getProject(prjName)

        let taskId = prj.issueTypes.find(el => el.name === 'Задача').id

        // console.log('taskId', taskId)
        // console.log('task', task)

        // let cf = await jira.createCustomField({
        //     searcherKey: "com.atlassian.jira.plugin.system.customfieldtypes:textsearcher",
        //     name: 'epicId',
        //     description: 'Parent Epic id',
        //     type: 'com.atlassian.jira.plugin.system.customfieldtypes:textfield'
        // })

        if (prj && task && taskId) {
            await jira.addNewIssue({
                fields: {
                    labels: [task.epicId],
                    summary: task.name,
                    description: {
                        type: 'doc',
                        version: 1,
                        content: [
                            {
                                type: 'paragraph',
                                content: [
                                    {
                                        text: task.description,
                                        type: 'text'
                                    }
                                ]
                            }
                        ]
                    },
                    issuetype: {
                        id: taskId,
                        name: 'Задача'
                    },
                    project: {
                        id: prj.id
                    }
                },
            })
        }

        // All issues of project
        const els = await request(`https://${jiraUrl}/rest/api/3/search?jql=project=${prjName}&maxResults=-1`, jiraName, jiraPass)

        res.json({ connected: true, els, prj, message: 'Success!' })
    } catch (e) {
        // console.log(`Что-то пошло не так! Ошибка: ${e.message}`)
        res.json({connected: false, message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// delete project by id
router.post('/deleteEpic', auth, async (req, res) => {
    const {  jiraUrl, jiraName, jiraPass, epicId } = req.body

    try {

        const jira = getJira(jiraUrl, jiraName, jiraPass)

        // this delete only epic don't delete tasks!!!
        await jira.deleteIssue(epicId)

        return res.status(202).json({ message: 'Эпик удалён' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

module.exports = router