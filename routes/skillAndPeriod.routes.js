const { Router } = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Skills = db.moduleSkill
const Periods = db.modulePeriod

const router = Router()


// route prefix = /api/skillAndPeriod

// create project



router.get('/skills/:id', auth, async (req, res) => {
    try {
        const skills = await Skills.findAll({
            where: { blockId: req.params.id }
        })
        res.json({ skills, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка get all: ${e.message}` })
    }
})


router.get('/periods/:id', auth, async (req, res) => {
    try {
        const periods = await Periods.findAll({
            where: { blockId: req.params.id }
        })
        res.json({ periods, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка get all: ${e.message}` })
    }
})



router.post('/skills/create', auth, async (req, res) => {
    try {
        const { blockId, skill, level, amount } = req.body

        const result = await Skills.create({ blockId, skill, level, amount })

        return res.status(201).json({ result, message: 'ok' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка create : ${e.message}` })
    }
})

router.post('/periods/create', auth, async (req, res) => {
    try {
        const { blockId, startDate, endDate } = req.body

        const result = await Periods.create({ blockId, startDate, endDate })

        return res.status(201).json({ result, message: 'ok' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка create : ${e.message}` })
    }
})



// update project by id
router.put('/skills/:id', auth, async (req, res) => {
    try {
        const { blockId, skill, level, amount } = req.body

        const result = await Skills.findByPk(req.params.id)

        if (result) {
            await result.update({ blockId, skill, level, amount })
        }

        return res.status(202).json({ result, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка update : ${e.message}` })
    }
})

router.put('/periods/:id', auth, async (req, res) => {
    try {
        const { blockId, startDate, endDate } = req.body

        const result = await Periods.findByPk(req.params.id)

        if (result) {
            await result.update({ blockId, startDate, endDate })
        }

        return res.status(202).json({ result, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка update : ${e.message}` })
    }
})







// delete project by id
router.delete('/skills/:id', auth, async (req, res) => {
    try {
        const result = await Skills.findByPk(req.params.id)

        await result.destroy()

        return res.status(202).json({ message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка delete : ${e.message}` })
    }
})


// delete project by id
router.delete('/periods/:id', auth, async (req, res) => {
    try {
        const result = await Periods.findByPk(req.params.id)

        await result.destroy()

        return res.status(202).json({ message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка delete : ${e.message}` })
    }
})









module.exports = router