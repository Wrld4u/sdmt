const { Router } = require('express')
const auth = require('../middleware/auth.middleware')
const fs = require('fs')
const db = require("../models")
const { Module } = require('module')
const Project = db.project
const Blocks = db.block
const Property = db.property
const Options = db.option
const Skill = db.skill
const Team = db.team
const User = db.user
const TeamUserSkill = db.teamUSkill
const EpicTeam = db.epicTeam

const ModuleSkill = db.moduleSkill
const ModulePeriod = db.modulePeriod

const router = Router()


// route prefix = /api/project

// create project
router.post('/create', auth, async (req, res) => {
    try {
        const { name, description } = req.body
        const id = req.headers.id

        const project = await Project.create({ name, description })

        // set User
        await project.setUsers([id])

        return res.status(201).json({ id: project.id, message: 'Проект создан' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// get project by id
router.get('/:id', auth, async (req, res) => {
    try {

        const project = await Project.findByPk(req.params.id)

        res.json({ project, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// get project by id with blocks
router.get('/blocks/:id', auth, async (req, res) => {
    try {

        const project = await Project.findOne({
            where: { id: req.params.id }, include: [
                {
                    model: Blocks,
                    include: [
                        {
                            model: Property,
                            include: [Options]
                        }
                    ]
                },
            ]
        })

        res.json({ project, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// get project by id with skills
// router.get('/skills/:id', auth, async (req, res) => {
//     try {

//         const project = await Project.findOne({
//             where: { id: req.params.id }, include: [
//                 {
//                     model: Skill,
//                     // include: [
//                     //     {
//                     //         model: Property,
//                     //         include: [Options]
//                     //     }
//                     // ]
//                 },
//             ], attributes: { exclude: ['archStr'] }
//         })

//         res.json({ project, message: 'ok' })
//     } catch (e) {
//         res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
//     }
// })








// // get project by id with skills/team + all users
// router.get('/team/:id', auth, async (req, res) => {
//     try {

//         const project = await Project.findOne({
//             where: { id: req.params.id }, include: [
//                 {
//                     model: Skill,
//                 },
//                 {
//                     model: Team,
//                     include: [
//                         {
//                             model: User,
//                             attributes: ['id', 'email', 'name']
//                             // include: [Options]
//                         },
//                         {
//                             model: TeamUserSkill,
//                             attributes: ['id', 'level', 'skillId'],
//                             include: [Skill]
//                         },
//                     ]
//                 },
//             ], attributes: { exclude: ['archStr'] }
//         })

//         const users = await User.findAll()

//         res.json({ project, users, message: 'ok' })
//     } catch (e) {
//         res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
//     }
// })







// get project by id with skills/team + all users
router.get('/team', auth, async (req, res) => {
    try {

        const project = await Project.findAll({
            include: [
                {
                    model: Skill,
                },
                {
                    model: Team,
                    include: [
                        {
                            model: User,
                            attributes: ['id', 'email', 'name']
                            // include: [Options]
                        },
                        {
                            model: TeamUserSkill,
                            attributes: ['id', 'level', 'skillId'],
                            include: [Skill]
                        },
                    ]
                },
            ], attributes: { exclude: ['archStr'] }
        })

        const users = await User.findAll()

        res.json({ project, users, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})








// get project by id with blocks/skill/team + all users
router.get('/roadmap/:id', auth, async (req, res) => {
    try {

        const project = await Project.findOne({
            where: { id: req.params.id }, include: [
                {
                    model: Blocks,
                    include: [
                        {
                            model: ModuleSkill
                        },
                        {
                            model: ModulePeriod
                        }
                    ]
                },
                {
                    model: Skill,
                },
                {
                    model: Team,
                    include: [
                        {
                            model: User,
                            attributes: ['id', 'email', 'name']
                            // include: [Options]
                        },
                        {
                            model: TeamUserSkill,
                            attributes: ['id', 'level', 'skillId'],
                            include: [Skill]
                        },
                        {
                            model: EpicTeam,
                            // attributes: ['id', 'epicId', 'teamId']
                            attributes: ['id', 'epicId']
                        },
                    ]
                },
                // ], attributes: { exclude: ['archStr'] }})
            ]
        })

        const users = await User.findAll()

        res.json({ project, users, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// get projects
router.post('/projects', auth, async (req, res) => {
    try {
        // const projects = await Project.findAll({include:[Cycle, Release]})
        const projects = await Project.findAll()
        res.json({ projects, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// update project by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { name, form } = req.body

        const project = await Project.findByPk(req.params.id)

        if (name) {
            project.name = name
        }

        if (form) {
            project.update({ ...form })
        }

        await project.save()

        return res.status(202).json({ project, message: 'Проект обновлён' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// update project by id
router.post('/archUpd', auth, async (req, res) => {
    try {
        const { id, archStr } = req.body

        const project = await Project.findByPk(id)

        if (project) {
            project.update({ archStr })
        }

        return res.status(202).json({ message: 'Проект обновлён' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// delete project by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const project = await Project.findByPk(req.params.id)

        // delete all blocks files!!!
        const blocks = await project.getBlocks()

        if (blocks.length) {
            blocks.forEach(b => {
                if (process.env.NODE_ENV === 'production') {
                    fs.rmdir(`./client/build/assets/snippet/${b.id}`, { recursive: true }, () => { })
                } else {
                    fs.rmdir(`./client/public/assets/snippet/${b.id}`, { recursive: true }, () => { })
                }
            })
        }

        await project.destroy()

        return res.status(202).json({ message: 'Проект удалён' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

module.exports = router