const { Router } = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Team = db.team
const TeamUserSkill = db.teamUSkill

const router = Router()


// route prefix = /api/team

// create user in team
router.post('/create', auth, async (req, res) => {
    try {
        const { projectId, userId, jiraUser, name } = req.body

        const team = await Team.create({ projectId: null, userName: name, userId: null, jiraUser })

        return res.status(201).json({ team, message: 'User matched' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// create skill for user
router.post('/addSkill', auth, async (req, res) => {
    try {
        const { teamId, skillId, level } = req.body

        const teamuSkill = await TeamUserSkill.create({ teamId, skillId, level })

        return res.status(201).json({ teamuSkill, message: 'Навык добавлен' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// // update project by id
// router.put('/:id', auth, async (req, res) => {
//     try {
//         const { projectId, name, jiraName } = req.body
//
//         const skill = await Skill.findByPk(req.params.id)
//
//         if (skill) {
//             skill.update({projectId, name, jiraName})
//         }
//
//         return res.status(202).json({ skill, message: 'Skill updated' })
//     } catch (e) {
//         res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
//     }
// })

// delete project by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const team = await Team.findByPk(req.params.id)

        await team.destroy()

        return res.status(202).json({ message: 'User match deleted' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})




router.get('/users', auth, async (req, res) => {
    try {
        // Получаем всех пользователей с их навыками
        const usersWithSkills = await Team.findAll({
            include: [
                {
                    model: db.teamUSkill, // Связь с таблицей teamUserSkill
                    include: [
                        {
                            model: db.skill, // Подключаем таблицу навыков
                            attributes: ['id', 'name'], // Указываем нужные поля навыков
                        },
                    ],
                },
            ],
        })

        res.json({ users: usersWithSkills, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка get users: ${e.message}` })
    }
})





// delete project by id
router.delete('/skill/:id', auth, async (req, res) => {
    try {
        const teamuSkill = await TeamUserSkill.findByPk(req.params.id)

        await teamuSkill.destroy()

        return res.status(202).json({ message: 'Навык удалён' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

module.exports = router