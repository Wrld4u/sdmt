const { Router } = require('express')
const auth = require('../middleware/auth.middleware')
const fs = require('fs')
const db = require("../models")
const { where } = require('sequelize')
const EpicTeam = db.epicTeam
const EpicK = db.epicK
const TeamUSkill = db.team


const router = Router()


// route prefix = /api/roadmap

// add user to epic team
router.post('/addUser', auth, async (req, res) => {
    try {
        const { epicId, teamId } = req.body

        const epT = await EpicTeam.create({ epicId, teamId })

        return res.status(201).json({ message: 'Пользователь добавлен' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e}` })
    }
})


router.get('/getUserAndSkill', auth, async (req, res) => {
    try {
        const teamUSkill = await TeamUSkill.findAll()


        console.log("teamUSkill", teamUSkill)
        return res.status(201).json({})

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})



router.get('/getTeam/:id', auth, async (req, res) => {

    const blockId = req.params.id
    try {
        const team = await EpicTeam.findAll({
            where: { epicId: blockId }
        })


        let tmpTeam = team.map(el => el.teamId)

        let teamUSkill = []

        if (tmpTeam?.length) {

            teamUSkill = await TeamUSkill.findAll({
                where: { teamId: tmpTeam }
            })


        }

        return res.status(201).json({ team: teamUSkill })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})





// delete user from epic team
router.delete('/deleteUser/:id', auth, async (req, res) => {
    try {
        const epT = await EpicTeam.findByPk(req.params.id)

        await epT.destroy()

        return res.status(202).json({ message: 'Пользователь удалён' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// add coefficient of team to epic
router.post('/addK', auth, async (req, res) => {
    try {
        const { epicId, coefficient } = req.body

        const epK = await EpicK.findOne({ where: { epicId } })

        if (epK) {
            await epK.update({ coefficient })
        } else {
            await EpicK.create({ epicId, coefficient })
        }

        return res.status(201).json({ message: 'Коэффициент добавлен' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// get coefficient of team to epic
router.get('/getK/:epicId', auth, async (req, res) => {
    try {
        const epK = await EpicK.findOne({ where: { epicId: req.params.epicId } })

        let k = 1

        if (epK) {
            k = epK.coefficient
        }

        return res.status(201).json({ k, message: 'Коэффициент' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

module.exports = router