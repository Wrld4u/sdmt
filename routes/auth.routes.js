const {Router} = require('express')
const bcrypt = require('bcryptjs')
const config = require('config')
const jwt = require('jsonwebtoken')
const {check, validationResult} = require('express-validator')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const User = db.user
const Prj = db.project

const router = Router()


// route prefix = /api/auth

// create user
router.post(
    '/register',
    [
        check('email', 'Некорректный email').isEmail(),
        check('password', 'Минимальная длина пароля 6 символов').isLength({ min: 6 })
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Некорректные данные при регистрации <br>' + errors.array().map(err => err.msg).join('<br>')
                })
            }

            const { email, password, name = ''} = req.body
            const candidate = await User.findOne({where:{ email }})
            if (candidate) {
                return res.status(400).json({ message: 'Такой пользователь уже существует' })
            }

            const hashedPassword = await bcrypt.hash(password, 10)
            const user = await User.create({ email, password: hashedPassword, name})

            // set role Member
            await user.setRoles([2])

            return res.status(201).json({ message: 'Пользователь создан' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
    })

// login user
router.post(
    '/login',
    [
        check('email', 'Некорректный email').normalizeEmail().isEmail(),
        check('password', 'Введите пароль').exists()
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Некорректные данные при авторизации'
                })
            }

            const { email, password } = req.body

            // let user = await User.findOne({where:{ email }, include: [{model: db.role}, {model: db.project}]})
            let user = await User.findOne({where:{ email }, include: [{model: db.role}]})
            const projects = await Prj.findAll()
            // console.log('projects', projects)

            if (!user) {
                return res.status(400).json({ message: 'Такого пользователя нет' })
            }

            user.dataValues.projects = projects
            
            const isMatch = await bcrypt.compare(password, user.password)
            if (!isMatch) {
                return res.status(400).json({ message: 'Неверный пароль' })
            }

            // console.log(await user.getRoles())

            const token = jwt.sign(
                { userId: user.id },
                config.get('jwtSecret'),
                // { expiresIn: '1h' }
                {}
            )

            res.json({ token, userId: user.id, user, message: 'Вы вошли' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
    })

// get user by id
router.get('/:id', auth, async (req, res) => {
    try {
        const user = await User.findByPk(req.params.id)
        res.json({ user, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// get user by email
router.post('/getWithProjetcs', auth, async (req, res) => {
    try {
        const { email } = req.body

        let user = await User.findOne({where:{ email }, include: [{model: db.role}]})
        const projects = await Prj.findAll()
        user.dataValues.projects = projects

        res.json({ user, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// updateProfile (name + email)
router.post(
    '/updateProfile', auth, async (req, res) => {
        try {
            const { email, name } = req.body

            const user = await User.findOne({where:{ email }})
            if (!user) {
                return res.status(400).json({ message: 'Такого пользователя нет' })
            }

            // update
            user.email = email
            user.name = name

            await user.save()

            res.json({ message: 'Обновлено' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
})

// updatePassword only password
router.post(
    '/updatePassword', auth, async (req, res) => {
        try {
            const { password, newPassword } = req.body
            const userId = req.headers.id

            const user = await User.findByPk(userId)
            if (!user) {
                return res.status(400).json({ message: 'Такого пользователя нет' })
            }

            // check pass is valid
            const isMatch = await bcrypt.compare(password, user.password)
            if (!isMatch) {
                return res.status(400).json({ message: 'Неверный пароль' })
            }

            const hashedPassword = await bcrypt.hash(newPassword, 10)

            // update
            // console.log(req.body)
            user.password = hashedPassword
            await user.save()

            res.json({ message: 'Обновлено' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
})

// upload avatar
router.post('/upload', auth, async (req, res) => {
    try {
        if(!req.files) {
            return res.status(400).json({ message: 'Файл не загружен' })
        } else {
            //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
            let avatar = req.files.avatar
            const userId = req.headers.id

            // Use the mv() method to place the file in upload directory (i.e. "uploads")
            if (process.env.NODE_ENV === 'production') {
                avatar.mv(`./client/build/assets/userImg/${userId}.jpg`)
            } else {
                avatar.mv(`./client/public/assets/userImg/${userId}.jpg`)
            }
            const user = await User.findByPk(userId)
            if (!user) {
                return res.status(400).json({ message: 'Такого пользователя нет' })
            }
            // console.log(avatar, userId)
            user.photo = `/assets/userImg/${userId}.jpg`
            await user.save()

            //send response
            // res.send({
            //     status: true,
            //     message: 'File is uploaded',
            //     data: {
            //         name: avatar.name,
            //         mimetype: avatar.mimetype,
            //         size: avatar.size
            //     }
            // });
            res.json({ message: 'Обновлено' })
        }
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// get all users
router.post('/users', auth, async (req, res) => {
    try {
        const users = await User.findAll({include: [{model: db.role}]})
        res.json({ users, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

router.post('/updateRole', auth, async (req, res) => {
    try {
        const { userId, role } = req.body

        const user = await User.findByPk(userId)
        if (!user) {
            return res.status(400).json({ message: 'Такого пользователя нет' })
        }

        // set role Member
        await user.setRoles([role])

        res.json({ message: 'Обновлено' })

    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

router.post('/updateNotify', auth, async (req, res) => {
    try {
        const { userId, notify } = req.body

        const user = await User.findByPk(userId)
        if (!user) {
            return res.status(400).json({ message: 'Такого пользователя нет' })
        }

        user.notify = notify.notify
        user.invite = notify.invite

        await user.save()

        res.json({user, message: 'Обновлено' })

    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

router.post('/check', auth, async (req, res) => {
    try {
        const { userId } = req.body

        const user = await User.findByPk(userId)
        if (!user) {
            return res.status(200).json({isUser: false, message: 'Такого пользователя нет' })
        }

        res.json({isUser: true, message: 'Обновлено' })

    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

module.exports = router