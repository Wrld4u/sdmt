const { Router } = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Skill = db.skill

const router = Router()


// route prefix = /api/skill

// create project
router.post('/create', auth, async (req, res) => {
    try {
        const { projectId, name, description, jiraName } = req.body

        const skill = await Skill.create({ projectId, name, jiraName: description })

        return res.status(201).json({ skill, message: 'Навык создан' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка create : ${e.message}` })
    }
})

// update project by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { projectId, name, description, jiraName } = req.body

        const skill = await Skill.findByPk(req.params.id)

        if (skill) {
            await skill.update({ projectId, name, jiraName: description })
        }

        return res.status(202).json({ skill, message: 'Навык обновлен' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка update : ${e.message}` })
    }
})

// delete project by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const skill = await Skill.findByPk(req.params.id)

        await skill.destroy()

        return res.status(202).json({ message: 'Навык удалён' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка delete : ${e.message}` })
    }
})




router.get('/all', auth, async (req, res) => {
    try {

        const project = await Skill.findAll()
        console.log("project", project)

        res.json({ skills: project, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка get all: ${e.message}` })
    }
})



module.exports = router