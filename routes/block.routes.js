const { Router } = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const fs = require('fs')
const path = require('path')
const AdmZip = require('adm-zip')
const Block = db.block
const Prop = db.property
const Opt = db.option

const Op = db.Sequelize.Op

const router = Router()

// Подключаем файл с переменной architectureExamples
const { architectureExamples } = require('../arch/archCodeForBlocksAndActions')


// route prefix = /api/block

// upload file
router.post('/upload', auth, async (req, res) => {
    try {
        if (!req.files) {
            return res.status(400).json({ message: 'Файл не загружен' })
        } else {
            //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
            let snippet = req.files.snippet

            const blockId = req.headers.blockid
            const propId = req.headers.propid
            const optId = req.headers.optid

            const opt = await Opt.findByPk(optId)

            // Use the mv() method to place the file in upload directory (i.e. "uploads")
            if (process.env.NODE_ENV === 'production') {
                await fs.rmdirSync(`./client/build/assets/snippet/${blockId}/${propId}/${optId}`, { recursive: true })
                snippet.mv(`./client/build/assets/snippet/${blockId}/${propId}/${optId}/${snippet.name}`)
                await opt.update({ path: `./client/build/assets/snippet/${blockId}/${propId}/${optId}/${snippet.name}` })
            } else {
                await fs.rmdirSync(`./client/public/assets/snippet/${blockId}/${propId}/${optId}`, { recursive: true })
                snippet.mv(`./client/public/assets/snippet/${blockId}/${propId}/${optId}/${snippet.name}`)
                await opt.update({ path: `./client/public/assets/snippet/${blockId}/${propId}/${optId}/${snippet.name}` })
            }

            res.json({ message: 'File Saved' })
        }
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// create block with props & opts (+files)
router.post('/create', auth, async (req, res) => {
    try {


        const { block, props } = req.body
        // console.log("block", block)
        // console.log("props", props)

        delete block.id
        const blck = await Block.create({ ...block })

        let p = []
        let o = []
        if (props?.length && blck) {
            for (let i = 0; i < props?.length; i++) {
                let pStart = props[i].id
                delete props[i].id
                props[i].blockId = blck.id

                const prop = await Prop.create({ ...props[i] })
                p.push({ start: pStart, id: prop.id })

                if (props[i].options.length && prop) {
                    for (let j = 0; j < props[i].options.length; j++) {
                        let oStart = props[i].options[j].id
                        delete props[i].options[j].id
                        delete props[i].options[j].path
                        props[i].options[j].propertyId = prop.id

                        const opt = await Opt.create({ ...props[i].options[j] })
                        o.push({ start: oStart, id: opt.id })
                    }
                }
            }
        }

        return res.status(201).json({ props: p, opts: o, blockId: blck.id, message: 'Block created' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// update block with props & opts (+files)
router.put('/:id', auth, async (req, res) => {
    try {
        const { block, props } = req.body
        let p = []
        let o = []

        const blck = await Block.findByPk(block.id)

        if (blck) {
            await blck.update({ ...block })

            let initProps = await blck.getProperties({ include: [Opt] })

            // let resProps = props?.filter(p => p.id > 0)
            let resProps = []


            // create new props & opts
            // find changes and update
            if (props?.length && blck) {
                for (let i = 0; i < props?.length; i++) {
                    // create new prop with options
                    if (props[i].id < 0) {
                        let pStart = props[i].id
                        delete props[i].id
                        props[i].blockId = blck.id

                        const prop = await Prop.create({ ...props[i] })
                        p.push({ start: pStart, id: prop.id })

                        if (props[i].options.length && prop) {
                            for (let j = 0; j < props[i].options.length; j++) {
                                let oStart = props[i].options[j].id
                                delete props[i].options[j].id
                                delete props[i].options[j].path
                                props[i].options[j].propertyId = prop.id

                                const opt = await Opt.create({ ...props[i].options[j] })
                                o.push({ start: oStart, id: opt.id })
                            }
                        }
                    } else {
                        // update existing property with options (maybe new) and files (maybe changed)
                        // compare with initProps (already saved to server)

                        // update existing property
                        const prop = await Prop.findByPk(props[i].id)
                        await prop.update({ ...props[i] })
                        p.push({ start: prop.id, id: prop.id })

                        // create new opts (if id < 0) with files
                        if (props[i].options.length && prop) {
                            for (let j = 0; j < props[i].options.length; j++) {
                                if (props[i].options[j].id < 0) {
                                    // create new opt
                                    let oStart = props[i].options[j].id
                                    delete props[i].options[j].id
                                    delete props[i].options[j].path
                                    props[i].options[j].propertyId = prop.id

                                    const opt = await Opt.create({ ...props[i].options[j] })
                                    o.push({ start: oStart, id: opt.id })
                                } else {
                                    // update existing opt
                                    delete props[i].options[j].path
                                    const opt = await Opt.findByPk(props[i].options[j].id)
                                    await opt.update({ ...props[i].options[j] })
                                    o.push({ start: opt.id, id: opt.id })
                                }
                            }
                        }
                    }
                }
            }

            // delete props then opts if not in new props and opts (with files). Compare props with initProps
            if (resProps?.length && initprops?.length) {
                let resPropsId = resProps?.map(r => r.id)

                for (let i = 0; i < initprops?.length; i++) {
                    if (!resPropsId.includes(initProps[i].id)) {
                        // delete property(initProps[i].id) + opts + files
                        // delete files
                        if (initProps[i].options.length) {
                            for (let j = 0; j < initProps[i].options.length; j++) {
                                if (initProps[i].options[j].path) {
                                    fs.rmdir(initProps[i].options[j].path, { recursive: true }, () => { })
                                }
                            }
                        }
                        await Prop.destroy({ where: { id: initProps[i].id } })
                    } else {
                        //check opts and if not exists - delete with file
                        if (initProps[i].options.length) {
                            let resProp = resProps?.find(el => el.id === initProps[i].id)
                            if (resProp && resProp.options.length) {
                                let resOptsId = resProp.options.map(el => el.id)

                                for (let j = 0; j < initProps[i].options.length; j++) {
                                    if (!resOptsId.includes(initProps[i].options[j].id)) {
                                        // delete current option(initProps[i].options[j].id) and file in this prop(initProps[i].id)
                                        // delete files
                                        if (initProps[i].options[j].path) {
                                            fs.rmdir(initProps[i].options[j].path, { recursive: true }, () => { })
                                        }
                                        await Opt.destroy({ where: { id: initProps[i].options[j].id } })
                                    }
                                }
                            } else {
                                // delete all options and files in this prop(initProps[i].id)
                                // delete files
                                if (initProps[i].options.length) {
                                    for (let j = 0; j < initProps[i].options.length; j++) {
                                        if (initProps[i].options[j].path) {
                                            fs.rmdir(initProps[i].options[j].path, { recursive: true }, () => { })
                                        }
                                    }
                                }
                                let pr = await Opt.destroy({ where: { propertyId: initProps[i].id } })
                            }
                        }
                    }
                }
            }

        }

        return res.status(202).json({ props: p, opts: o, blockId: blck.id, message: 'Block updated' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// delete block with props & opts (+files)
router.delete('/:id', auth, async (req, res) => {
    try {
        const block = await Block.findByPk(req.params.id)

        await block.destroy()

        if (process.env.NODE_ENV === 'production') {
            fs.rmdir(`./client/build/assets/snippet/${req.params.id}`, { recursive: true }, () => { })
        } else {
            fs.rmdir(`./client/public/assets/snippet/${req.params.id}`, { recursive: true }, () => { })
        }

        return res.status(202).json({ message: 'Block deleted' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// update selected option in block->property
router.post('/propUpd', auth, async (req, res) => {
    try {
        const { optId, propId } = req.body

        const prop = await Prop.findByPk(propId)

        if (prop) {
            prop.update({ selectedId: optId })
        }

        return res.status(202).json({ message: 'Option selected' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// zip block files
// router.post('/zip', auth, async (req, res) => {
//     try {
//         const { blockIds } = req.body

//         const rmSpace = (text) => {
//             return text.trim().replace(' ', '_')
//         }

//         let relPath = ''
//         let absPath = ''
//         let tmpDir = String(+new Date())
//         if (process.env.NODE_ENV === 'production') {
//             relPath = `./client/build/assets/zips/${tmpDir}/`
//             absPath = `${path.dirname(require.main.filename)}/client/build/assets/zips/${tmpDir}`
//             // blockId/propId/optId
//         } else {
//             relPath = `./client/public/assets/zips/${tmpDir}/`
//             absPath = `${path.dirname(require.main.filename)}/client/public/assets/zips/${tmpDir}`
//         }

//         let snips = []
//         // for each block in blockIds, get all files and zip it
//         if (blockIds.length) {
//             for (let i=0; i<blockIds.length; i++) {
//                 let bl = await Block.findByPk(blockIds[i])
//                 let props = await Prop.findAll({where: {blockId: blockIds[i], type: 'Select'}, include: [
//                     {
//                         model: Opt,
//                         where: {}
//                     }
//                     ]})

//                 if (props?.length) {
//                     props?.forEach(p => {
//                         if (p.options.length) {
//                             p.options.forEach(o => {
//                                 if (o.id === p.selectedId && o.path) snips.push({blockName: rmSpace(bl.name), propName: rmSpace(p.name), optText: rmSpace(o.text), fl: o.path, fName: o.file})
//                             })
//                         }
//                     })
//                 }
//             }
//         }

//         // let readyToZip = []
//         if (snips.length) {
//             fs.mkdirSync(relPath)
//             for (let i=0; i<snips.length; i++) {
//                 let newFile = `${relPath}${snips[i].blockName}_${snips[i].propName}_${snips[i].optText}_${snips[i].fName}`
//                 fs.copyFileSync(snips[i].fl, newFile)
//                 // readyToZip.push(newFile)
//             }
//         }

//         // console.log('absPath', absPath)

//         // nodeJsZip.zip(absPath, {dir: absPath, name: `snippets_${tmpDir}`})
//         const zip = new AdmZip()
//         zip.addLocalFolder(absPath, tmpDir)
//         zip.writeZip(`${absPath}/snippets_${tmpDir}.zip`)

//         let dirPath = path.join('assets', 'zips', tmpDir)

//         return res.status(202).json({ dir: tmpDir, path: `${dirPath}/snippets_${tmpDir}.zip`, fName: `snippets_${tmpDir}.zip`, message: 'file created' })
//         // return res.status(202).json({ readyToZip, message: 'file created' })

//     } catch (e) {
//         res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
//     }
// })




function findProjectFilesByNodeId(nodeId) {
    for (const example of architectureExamples) {
        for (const block of example.blocks.blocks) { // Доступ к массиву blocks
            if (block.nodeId === nodeId) {
                return block.projectFiles
            }
        }
        for (const action of example.blocks.action) { // Доступ к массиву action
            if (action.nodeId === nodeId) {
                return action.projectFiles
            }
        }
    }
    return [
        {
            "name": "index.js",
            "content": `// blank template`
        },

        {
            "name": "package.json",
            "content": JSON.stringify({
                "name": "blank template",
                "version": "1.0.0",
                "description": "blank template",
                "main": "index.js",
                "scripts": {
                    "start": "node index.js"
                },
                "author": "Your Name",
                "license": "ISC"
            }, null, 2)
        },
        {
            "name": "README.md",
            "content": "# blank template."
        }
    ] // Если nodeId не найден
}


router.post('/zip', auth, async (req, res) => {

    // console.log("architectureExamples", architectureExamples)
    try {
        const { block } = req.body
        const { nodeId } = block

        // console.log('req.body', nodeId); // Проверка nodeId

        const projectFiles = findProjectFilesByNodeId(nodeId)

        if (!projectFiles) {
            return res.status(404).json({ message: 'Файлы проекта не найдены для данной ноды' })
        }

        let tmpDir = String(+new Date())

        let relPath = ''
        let absPath = ''
        if (process.env.NODE_ENV === 'production') {
            relPath = `./client/build/assets/zips/${tmpDir}/`
            absPath = path.join(path.dirname(require.main.filename), 'client', 'build', 'assets', 'zips', tmpDir)
        } else {
            relPath = `./client/public/assets/zips/${tmpDir}/`
            absPath = path.join(path.dirname(require.main.filename), 'client', 'public', 'assets', 'zips', tmpDir)
        }

        fs.mkdirSync(relPath, { recursive: true })

        projectFiles.forEach(file => {
            const filePath = path.join(relPath, file.name)
            fs.writeFileSync(filePath, file.content)
        })

        const zip = new AdmZip()
        zip.addLocalFolder(absPath)
        zip.writeZip(`${absPath}/snippets_${tmpDir}.zip`)

        let dirPath = path.join('assets', 'zips', tmpDir)

        return res.status(202).json({
            dir: tmpDir,
            path: `${dirPath}/snippets_${tmpDir}.zip`,
            fName: `snippets_${tmpDir}.zip`,
            message: 'file created'
        })

    } catch (e) {
        console.log("Error - ", e)
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})



// download zip (not working yet)
router.post('/zipdl/:dir', auth, async (req, res) => {
    try {

        const dir = req.params.dir

        // console.log('dir', dir)

        let absPath = ''
        let tmpDir = String(req.params.dir)
        if (process.env.NODE_ENV === 'production') {
            absPath = `${path.dirname(require.main.filename)}/client/build/assets/zips/${tmpDir}`
        } else {
            absPath = `${path.dirname(require.main.filename)}/client/public/assets/zips/${tmpDir}`
        }

        await fs.rmdirSync(absPath, { recursive: true })

        res.status(202).json({ message: 'Архив удален' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

module.exports = router