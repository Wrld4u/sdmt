import React, {useCallback, useContext, useEffect, useState} from 'react'
import {AuthContext} from "../../context/AuthContext"
import {Link, useHistory, useParams} from "react-router-dom"
import {useMessage} from "../../hooks/message.hook"
import {useHttp} from "../../hooks/http.hook"
import {Loader} from "../partials/Loader"
import {Header} from "../partials/Header"
import {Icon} from "../partials/Icon"
import {flowy} from '../../flowy/flowy'

export const ArchPage = () => {
    const id = useParams().id
    const { token, logout, user } = useContext(AuthContext)
    const history = useHistory()
    const {loading, request, error, clearError} = useHttp()
    const message = useMessage()
    const [project, setProject] = useState(null)

    // const [rightcard, setRightcard] = useState(false)
    // const [tempblock, setTempblock] = useState(null)
    const [tempblock2, setTempblock2] = useState(null)
    // const [grab, setGrab] = useState(null)
    // const [blockin, setBlockin] = useState(null)
    const [canvas, setCanvas] = useState(null)
    const [chart, setChart] = useState(null)
    const [action, setAction] = useState('Action')

    useEffect(() => {
        message(error)
        clearError()
    }, [error, message, clearError, logout, history])

    // Активация input для materialize
    useEffect(() => {
        window.M.updateTextFields()
        if (document.querySelectorAll('select')) window.M.FormSelect.init(document.querySelectorAll('select'))
        if (document.getElementById('description')) window.M.textareaAutoResize(document.getElementById('description'))
    })

    // useEffect(() => {
    //     if (canvas && project && !loading) {
    //         let rightcard = false
    //         let tempblock
    //         let tempblock2
    //
    //         flowy(canvas, drag, release, snapping)
    //
    //         if (project.archStr && !flowy.output()) {
    //             flowy.import(JSON.parse(project.archStr))
    //         }
    //
    //         function drag(block) {
    //             block.classList.add("blockdisabled")
    //             tempblock2 = block
    //         }
    //
    //         function release() {
    //             if (tempblock2) {
    //                 tempblock2.classList.remove("blockdisabled")
    //             }
    //
    //             setTimeout(async () => {
    //                 try {
    //                     if (token && id) {
    //                         const data = await request(`/api/project/archUpd`, 'POST', {id: project.id, archStr: JSON.stringify(flowy.output())}, {authorization: 'Bearer ' + token})
    //                         // setProject(data.project)
    //                     }
    //                 } catch (e) {
    //                     console.log(e)
    //                 }
    //             }, 100)
    //
    //             setTimeout(() => {
    //                 console.log(JSON.stringify(flowy.output()))
    //             }, 100)
    //         }
    //
    //         function snapping(drag, first) {
    //             let grab = drag.querySelector(".grabme")
    //             grab.parentNode.removeChild(grab)
    //             let blockin = drag.querySelector(".blockin")
    //             blockin.parentNode.removeChild(blockin)
    //             if (drag.querySelector(".blockelemtype").value == "1") {
    //                 drag.innerHTML += "<div class='blockyleft'><p class='blockyname'>New visitor</p></div><div class='blockyright'><img src='assets/more.svg'></div><div class='blockydiv'></div><div class='blockyinfo'>When a <span>new visitor</span> goes to <span>Site 1</span></div>"
    //             } else if (drag.querySelector(".blockelemtype").value == "2") {
    //                 drag.innerHTML += "<div class='blockyleft'><p class='blockyname'>Action is performed</p></div><div class='blockyright'><img src='assets/more.svg'></div><div class='blockydiv'></div><div class='blockyinfo'>When <span>Action 1</span> is performed</div>"
    //             }
    //
    //             // setTimeout(() => {
    //             //     console.log(JSON.stringify(flowy.output()))
    //             // }, 100)
    //
    //             return true
    //         }
    //
    //     }
    // }, [canvas])

    useEffect(async () => {
        try {
            if (token && id) {
                // const data = await request(`/api/project/archUpd`, 'POST', {id, archStr: chart}, {authorization: 'Bearer ' + token})
                // setProject(data.project)
                console.log('SAVING ARCH')
            }
        } catch (e) {
            console.log(e)
        }
    }, [request, chart, id, token])

    const drag = useCallback((block) => {
        // block.classList.add("blockdisabled")
        // setTempblock2(block)
    }, [canvas])

    const release = useCallback(() => {
        // if (tempblock2) {
        //     tempblock2.classList.remove("blockdisabled")
        // }
        setTimeout(() => {
            console.log(JSON.stringify(flowy.output()), JSON.stringify(flowy.output()).length)
            if (JSON.stringify(flowy.output())) setChart(JSON.stringify(flowy.output()))
        }, 100)
    }, [canvas])

    const snapping = useCallback( (drag, first) => {

        return true
    }, [canvas])


    const getProject = useCallback(async (token, id) => {
        try {
            if (token && id) {
                const data = await request(`/api/project/blocks/${id}`, 'GET', null, {authorization: 'Bearer ' + token})
                setProject(data.project)
                setChart(data.project.archStr)
            }
        } catch (e) {
            console.log(e)
        }
    }, [request])

    useEffect(() => {
        // prj.toggle(true, id)
        getProject(token, id)

    }, [getProject, id, token])

    useEffect(() => {
        if (!canvas && document.getElementById("canvas")) {
            setCanvas(document.getElementById("canvas"))
        }
        if (canvas && project) {
            console.log('CREATE FLOWY')
            flowy(canvas, drag, release, snapping)
            if (project.archStr && !flowy.output()) {
                flowy.import(JSON.parse(project.archStr))
                console.log('SET Project ARCH')
            }
        }
    }, [canvas, drag, release, snapping, project])

    // useEffect(() => {
    //     console.log('project', project)
    // }, [project])


    if (!project || loading) {
        return <Loader />
    }

    return (
        <>
            <Header params={{
                title: `Модули`,
                subTitle: 'Управление модулями',
                bk: [
                    {
                        title: project ? project.name : '',
                        // actionHandler: () => {history.goBack()}
                        actionHandler: () => {}
                    },
                    {
                        title: `Архитектура`,
                        actionHandler: () => {}
                    },
                ],
                btnL: {
                    actionHandler: ()=>{console.log('left')},
                    title: 'btnLeft',
                    display: 'none'
                },
                btnR: {
                    actionHandler: () => {},
                    title: 'Создать модуль',
                    display: 'none'
                },
                loading
            }}/>

        <div className="container-fluid tilebg" style={{position: 'fixed', left: '240px', top: '65px', right: 0, height: '100%'}}>
            <div className="row clear-row my-0 py-0">
                <div className="col s12">

                    <div id="leftcard">

                        <p id="header" className="py-0">Модули</p>

                        <div id="search" className="pt-1">
                            <div className="input-field ml-0 pl-0 my-0 text-gray">
                                <i className="fa fa-search prefix" aria-hidden="true" style={{width: '24px', height: '15px', fontSize: 'inherit', marginTop: '5px'}}/>
                                <input
                                    placeholder="Поиск"
                                    type="text"
                                    // id="search"
                                    className=""
                                    style={{marginLeft: '24px', width: 'calc(100% - 30px)', borderBottom: 'none'}}
                                    name="search"
                                    autoComplete="off"
                                    onKeyPress={(key) => {
                                        // if (key.key === 'Enter') {
                                        //     history.push(`/`)
                                        //     history.push(`/search/${key.target.value}`)
                                        // }
                                    }}
                                />
                            </div>
                        </div>

                        <div id="subnav" className="d-flex justify-between align-center">
                            <div id="actions" className={`side d-flex justify-center ${action === 'Action' ? 'navactive' : 'navdisabled'}`} onClick={() => {setAction('Action')}}>
                                <div className="d-flex justify-center align-center">
                                    <Icon name='flash' size='15px' mt='2px'/>
                                    <span>Действие</span>
                                </div>
                            </div>
                            <div id="loggers" className={`side d-flex justify-center ${action === 'Block' ? 'navactive' : 'navdisabled'}`} onClick={() => {setAction('Block')}}>
                                <div className="d-flex justify-center align-center">
                                    <Icon name='box' size='15px' mt='2px'/>
                                    <span>Модули</span>
                                </div>
                            </div>
                        </div>

                        <div id="blocklist">
                            {project && project.blocks.length ? project.blocks.map(b => { if (b.type === action) return (
                                <React.Fragment key={b.id}>
                                <div className="blockelem create-flowy noselect">
                                    <input type="hidden" name='blockelemtype' className="blockelemtype" value={b.id} />
                                    <div className="grabme">
                                        {action === 'Action' ? <Icon name='flash' size='20px' mt='-6px'/> : <Icon name='box' size='20px' mt='-6px'/>}
                                    </div>
                                    <div className="blockin d-flex flex-column">
                                        <div className="blocktext">
                                            <p className="blocktitle">{b.name}</p>
                                            <p className="blockdesc">{b.description}</p>
                                        </div>
                                        <div>
                                            <button onClick={() => {console.log('clicked:', b.id)}}>btn</button>
                                        </div>
                                    </div>
                                </div>
                                </React.Fragment>)
                                }) : <></>}
                        </div>

                        <div id="footer" className="d-flex justify-center align-center">
                            <div className='addBtn txt-gray'>
                                + add block
                            </div>
                        </div>

                    </div>


                    <div id="propwrap">
                        <div id="properties">
                            <div id="close">
                                <p>&times;</p>
                            </div>
                            <p id="header2">Properties</p>
                            <div id="propswitch">
                                <div id="dataprop">Data</div>
                                <div id="alertprop">Alerts</div>
                                <div id="logsprop">Logs</div>
                            </div>
                            <div id="proplist">
                                {/*<p className="inputlabel">Select database</p>*/}
                                {/*<div className="dropme">Database 1 <img src="assets/dropdown.svg" /></div>*/}
                                {/*<p className="inputlabel">Check properties</p>*/}
                                {/*<div className="dropme">All<img src="assets/dropdown.svg" /></div>*/}
                                {/*<div className="checkus"><img src="assets/checkon.svg" /><p>Log on successful performance</p></div>*/}
                                {/*<div className="checkus"><img src="assets/checkoff.svg" /><p>Give priority to this block</p></div>*/}
                            </div>
                            <div id="divisionthing"></div>
                            <div id="removeblock">Удалить модули</div>
                        </div>
                    </div>
                    <div id="canvas">
                    </div>

                </div>
            </div>
        </div>
        </>
    )
}