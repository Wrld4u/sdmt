import React from 'react'
import {Loader} from "./Loader"

export const ColorProgress = ({props}) => {

    // props = [
    //     {
    //         name: 'Apples',
    //         value: 60,
    //         color: '#eb4d4b'
    //     },
    //     {
    //         name: 'Blueberries',
    //         value: 7,
    //         color: '#22a6b3'
    //     },
    //     {
    //         name: 'Guavas',
    //         value: 23,
    //         color: '#6ab04c'
    //     },
    //     {
    //         name: 'Grapes',
    //         value: 10,
    //         color: '#e056fd'
    //     }
    // ]


    if (!props) {
        return <Loader />
    }

    return (
        <div className="multicolor-bar">
            <div className="values">
                {props && props.length ? props.map((item, i) => {
                    if(item.value > 0) {
                        return (
                            <div className="value" style={{'color': item.color, 'width': item.value + '%'}}  key={i}>
                                <span>{item.value}%</span>
                            </div>
                        )
                    }
                }) : (<></>)}
            </div>
            <div className="scale">
                {props && props.length ? props.map((item, i) => {
                    if(item.value > 0) {
                        return (
                            <div className="graduation" style={{'color': item.color, 'width': item.value + '%'}}  key={i}>
                                <span>|</span>
                            </div>                    )
                    }
                }) : (<></>)}
            </div>
            <div className="bars">
                {props && props.length ? props.map((item, i) => {
                    if(item.value > 0) {
                        return (
                            <div className="bar" style={{'backgroundColor': item.color, 'width': item.value + '%'}}  key={i}>

                            </div>
                        )
                    }
                }) : (<></>)}
            </div>
            <div className="legends">
                {props && props.length ? props.map((item, i) => {
                    if(item.value > 0) {
                        return (
                            <div className="legend" key={i}>
                                <span className="dot" style={{'color': item.color}}>●</span>
                                <span className="label">{item.name}</span>
                            </div>
                        )
                    }
                }) : (<></>)}
            </div>
        </div>
    )
}