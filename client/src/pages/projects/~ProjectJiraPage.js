import React, {useCallback, useContext, useEffect, useState} from 'react'
import {useHistory, useParams} from "react-router-dom"
// import {SidebarContext} from "../../context/SidebarContext"
import {Loader} from "../partials/Loader"
import {AuthContext} from "../../context/AuthContext"
import {useHttp} from "../../hooks/http.hook"
import {useMessage} from "../../hooks/message.hook"

export const ProjectJiraPage = () => {
    const { token, user, logout, login } = useContext(AuthContext)
    const {loading, request, error, clearError} = useHttp()
    const id = useParams().id
    // const prj = useContext(SidebarContext)
    const [project, setProject] = useState(null)
    const message = useMessage()
    const history = useHistory()
    const [form, setForm] = useState({
        jiraUrl: '',
        jiraName: '',
        jiraPass: '',
        jiraProjectBoard: '0',
        jiraProject: '',
        jiraHRProjectBoard: '0',
        jiraHRProject: ''
    })
    const [boards, setBoards] = useState([])
    const [projects, setProjects] = useState([])
    const [projectsHR, setProjectsHR] = useState([])
    const [connected, setConnected] = useState(false)

    useEffect(() => {
        message(error)
        clearError()
    }, [error, message, clearError, logout, history])

    // Активация input для materialize
    useEffect(() => {
        window.M.updateTextFields()
        window.M.FormSelect.init(document.querySelectorAll('select'))
    })

    const getProject = useCallback(async (token, id) => {
        try {
            if (token && id) {
                const data = await request(`/api/project/${id}`, 'GET', null, {authorization: 'Bearer ' + token})
                setProject(data.project)
                setForm({
                    jiraUrl: data.project.jiraUrl || '',
                    jiraName: data.project.jiraName || '',
                    jiraPass: data.project.jiraPass || '',
                    jiraProjectBoard: data.project.jiraProjectBoard || 0,
                    jiraProject: data.project.jiraProject || '',
                    jiraHRProjectBoard: data.project.jiraHRProjectBoard || 0,
                    jiraHRProject: data.project.jiraHRProject || ''
                })

            }
        } catch (e) {
            console.log(e)
        }
    }, [request])

    const getProjects = useCallback(async (token, form, hr=null) => {
        try {
            console.log('get Jira projects form', form)
            const data = await request(`/api/jira/projects`, 'POST', {...form, hr}, {authorization: 'Bearer ' + token})
            // message(data.message)
            console.log(data)
            if (!hr) {
                setProjects(data.projects)
                // if (!form.jiraProject)
                    setForm(prev => {return {...prev, jiraProject: data.projects[0].key}})
            } else {
                setProjectsHR(data.projects)
                // if (!form.jiraHRProject)
                    setForm(prev => {return {...prev, jiraHRProject: data.projects[0].key}})
            }

        } catch (e) {
            console.log(e)
        }
    }, [request])

    useEffect(() => {
        // prj.toggle(true, id)
        getProject(token, id)

    }, [getProject, id, token])

    // update projects on change board
    useEffect(() => {
        if (connected && form.jiraProjectBoard && form.jiraProjectBoard !== '0') getProjects(token, form)
    }, [form.jiraProjectBoard, token])

    // update projects on change board HR
    useEffect(() => {
        if (connected && form.jiraHRProjectBoard && form.jiraHRProjectBoard !== '0') getProjects(token, form, 'hr')
    }, [form.jiraHRProjectBoard, token])

    useEffect(() => {
        console.log('form', form)
        console.log('boards', boards)
        console.log('projects', projects)
        console.log('projectsHR', projectsHR)
    }, [form, boards, projectsHR, projects])

    const changeHandler = event => {
        setForm(prev => { return {...prev, [event.target.name]: event.target.value} })
    }

    const updateHandler = async () => {
        try {
            const data = await request(`/api/project/${id}`, 'PUT', {form}, {authorization: 'Bearer ' + token})
            message(data.message)
        } catch (e) {}
    }

    const connectHandler = async () => {
        try {
            const data = await request(`/api/jira/boards`, 'POST', {...form}, {authorization: 'Bearer ' + token})
            if (data.connected) {
                setBoards(data.boards)
                setConnected(true)
                if (!form.jiraHRProjectBoard) {
                    setForm(prev => {return {...prev, jiraHRProjectBoard: data.boards[0].id}})
                } else {
                    getProjects(token, form, 'hr')
                }
                if (!form.jiraProjectBoard) {
                    setForm(prev => {return {...prev, jiraProjectBoard: data.boards[0].id}})
                } else {
                    getProjects(token, form)
                }
            }
            message(data.message)
        } catch (e) {
            console.log(e)
        }
    }

    if (!project || loading) {
        return <Loader />
    }

    return (
        <>
            <div className="row clear-row mt-noHeader">
                <div className="col-auto">
                    <h5>Jira integration - {project.name}</h5>
                    <p className="txt-gray">Connect project to Jira</p>
                </div>
            </div>

            <div className="row clear-row flex-row">
                <div className="col s4 ml-0 pl-0">
                    <div className="input-field">
                        <input
                            type="text"
                            id="url"
                            className=""
                            name="jiraUrl"
                            value={form.jiraUrl}
                            onChange={changeHandler}
                        />
                        <label htmlFor="url">Server URL</label>
                    </div>

                    <div className="input-field">
                        <input
                            type="text"
                            id="username"
                            className=""
                            name="jiraName"
                            value={form.jiraName}
                            onChange={changeHandler}
                        />
                        <label htmlFor="username">Username</label>
                    </div>

                    <div className="input-field">
                        <input
                            type="password"
                            id="password"
                            className=""
                            name="jiraPass"
                            value={form.jiraPass}
                            onChange={changeHandler}
                        />
                        <label htmlFor="password">Password</label>
                    </div>

                    {/*Save changes*/}
                    <button
                        className="waves-effect waves-light btn blue darken-1 noUpper mr-1"
                        onClick={updateHandler}
                        disabled={loading || !connected}
                    >
                        Connect
                    </button>

                    {/*Test connection*/}
                    <button
                        style={{border: '1px solid grey', color: 'black'}}
                        className="waves-effect waves-dark btn white lighten-1 noUpper"
                        onClick={connectHandler}
                        disabled={loading}
                    >
                        Test Connection
                    </button>

                </div>
            </div>

            <div className="row clear-row mt-3" style={!connected ? {display: 'none'} : {}}>
                <div className="col-auto">
                    <p style={{fontSize: '1.3rem'}}>Project settings</p>
                </div>
            </div>

            <div className="row clear-row flex-row" style={!connected ? {display: 'none'} : {}}>
                <div className="col s4 ml-0 pl-0">
                    {/*Jira jiraProjectBoard*/}
                    <div className="input-field">
                        <select
                            id="jiraProjectBoard"
                            name="jiraProjectBoard"
                            value={form.jiraProjectBoard}
                            onChange={changeHandler}
                        >
                            { boards.length ? boards.map(b => <option key={b.id} value={b.id}>{b.name}</option>) : (<option value="0">-</option>)}

                        </select>
                        <label htmlFor="jiraProjectBoard">Select board</label>
                    </div>

                    {/*Jira HR jiraProjectBoard*/}
                    <div className="input-field">
                        <select
                            id="jiraHRProjectBoard"
                            name="jiraHRProjectBoard"
                            value={form.jiraHRProjectBoard}
                            onChange={changeHandler}
                        >
                            { boards.length ? boards.map(b => <option key={b.id} value={b.id}>{b.name}</option>) : (<option value="0">-</option>)}
                        </select>
                        <label htmlFor="jiraHRProjectBoard">Select HR board</label>
                    </div>
                </div>

                <div className="col s4 ml-0 pl-0">
                    {/*Jira Project*/}
                    <div className="input-field">
                        <select
                            id="jiraProject"
                            name="jiraProject"
                            value={form.jiraProject}
                            onChange={changeHandler}
                        >
                            { projects.length ? projects.map(p => <option key={p.id} value={p.key}>{p.name}</option>) : (<option value="0">-</option>)}
                        </select>
                        <label htmlFor="jiraProject">Select project</label>
                    </div>

                    {/*Jira HR Project*/}
                    <div className="input-field">
                        <select
                            id="jiraHRProject"
                            name="jiraHRProject"
                            value={form.jiraHRProject}
                            onChange={changeHandler}
                        >
                            { projectsHR.length ? projectsHR.map(p => <option key={p.id} value={p.key}>{p.name}</option>) : (<option value="0">-</option>)}
                        </select>
                        <label htmlFor="jiraHRProject">Select HR project</label>
                    </div>
                </div>
            </div>
        </>
    )

}