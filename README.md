## Настройка jira
1. Регистрация [jira](https://www.atlassian.com/ru/software/jira) аккаунта  
    1.1. Jira Service Management  
    1.2. Заполнение регистрационных данных  
    1.3. Создать аккаунт, одновременно и URL к нему your_company_or_project_name.atlassian.net  
2. Создание и настройка проектов (Основной + HR)  
    2.1. Переходим на Jira Software -> Проекты -> Справа синяя кнопка "Создать проект"  
    2.2. Выбираем Попробовать проект, управляемый командой  
    2.3. Даём название проекту, шаблон Scrum нажимаем кнопку "Создать"  
    2.4. В проекте выбираем Настройки проекта -> Особенности -> отключаем Бэклог  
    2.5. Настройки проекта -> Типы задач -> Эпик. Добавляем в контектстные поля "Исходная оценка" и "Учёт времени"  
    2.6. Настройки проекта -> Типы задач -> Задача(Task). Добавляем в контектстные поля "Срок исполнения", "Исходная оценка", "Учёт времени", "Start date"  
      
3.  Получение ключа  
    3.1. Справа сверху нажмите на аватар, в выпадающем меню выберите "Настройки аккаунта"  
    3.2. Безопасность -> Токен API -> Создание токенов API и управление ими  
    3.3. Сверху кнопка "Создать токен API", обязательно сохраните полученный токен, далее посмотреть его не получится и придётся создавать новый  
  
## SDMT  
  
  Порядок работ:
  1. Создаётся проект в SDMT
  2. Integrations - заполняем форму подключения к jira  
    2.1 Server URL: из пункта 1.3. настройки jira  
    2.2 Username: email на который зарегистрирован аккаунт jira  
    2.3 Password: API токен из пункта 3.3. настройка jira
  3. Test connection -> появляется возможность выбрать основной и HR проекты в jira, после выбора Connect связывает эти проеты с проектом SDMT
  3. Blocks - Создание всех необходимых блоков 
  4. Architecture - создание наглядной схемы взаимодействия блоков (именно эти блоки схемы станут эпиками в проекте)
  5. Roadmap -> Create roadmap создаст в рабочем проекте jira набор эпиков соответствующих блокам использованным для создания схемы в Architecture
  6. Заполняем эпики задачами в jira (со всеми необходимыми полями и метками)
  7. Skills связываем skill SDMT со скиллами jira
  8. Team Skills связываем пользователей SDMT и пользователей Jira и добавляем им скиллы с уровнями
  9. В Roadmap создаём команды для эпиков и закрываем скиллами команды скиллы эпиков, при необходимости, здесь же создаём запросы на персонал - в HR проекте jira будут создаваться соответстующие таски  
  
    
**Requrement**
- node.js v14.15.1
- npm v 7.5.4
- MySQL 10.2.23-MariaDB
- pm2 manager (https://pm2.keymetrics.io/)

**Install**

1. Clone project to your directory
2. In project root run: \
 2.1 npm install \
 2.2 npm run client:install \
 2.3 npm run client:build 
 
**Run** 
- To run project (in project directory): pm2 start site.config.js

**Config**

_site.config.js_ 
1. env: Environment variables, here you can set Port of you application.
2. instances: Here you can set count of processor cores to use. \
more details - https://pm2.keymetrics.io/

_projectDir/config/production.json_ - in this file you can set DB settings, alternative port for application, base URL and jwt Token

    