// Запуск этого приложения: pm2 start site.config.js

module.exports = {
    apps : [{
        name: "skolkovo SDMT",
        script: "./app.js",
        env: {
            NODE_ENV: "production",
            PORT: 5002
        },
        env_dev: {
            NODE_ENV: "production",
            PORT: 5002
        },
        // instances : "2",
        // exec_mode : "cluster"
    }]
}
