module.exports = (sequelize, Sequelize) => {
    const TeamUserSkill = sequelize.define("teamUserSkill", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        teamId: {
            type: Sequelize.INTEGER,
        },
        skillId: {
            type: Sequelize.INTEGER,
        },
        level: {
            type: Sequelize.INTEGER
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate : Sequelize.NOW,
        },
    })

    return TeamUserSkill
}