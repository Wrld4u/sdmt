module.exports = (sequelize, Sequelize) => {
    const Project = require('./project.model')(sequelize, Sequelize)
    const Skill = sequelize.define("skills", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        projectId: {
            type: Sequelize.INTEGER,
            references: {
                model: Project,
                key: 'id'
            }
        },
        name: {
            type: Sequelize.STRING
        },
        jiraName: {
            type: Sequelize.STRING,
            // defaultValue: 0
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate : Sequelize.NOW,
        },
    })

    return Skill
}