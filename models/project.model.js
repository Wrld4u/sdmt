module.exports = (sequelize, Sequelize) => {
    const SequelizeSlugify = require('sequelize-slugify')

    const Project = sequelize.define("projects", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        slug: {
            type: Sequelize.STRING,
            unique: true
        },
        description: {
            type: Sequelize.STRING
        },
        companyId: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        },
        jiraUrl: {
            type: Sequelize.STRING
        },
        jiraName: {
            type: Sequelize.STRING
        },
        jiraPass: {
            type: Sequelize.STRING
        },

        // jiraProjectBoard: {
        //     type: Sequelize.STRING
        // },
        jiraProject: {
            type: Sequelize.STRING
        },

        // jiraHRProjectBoard: {
        //     type: Sequelize.STRING
        // },
        jiraHRProject: {
            type: Sequelize.STRING
        },

        archStr: {
            type: Sequelize.TEXT
        },

        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate : Sequelize.NOW,
        },
    })

    SequelizeSlugify.slugifyModel(Project, {
        source: ['name', 'id']
    })

    return Project
}