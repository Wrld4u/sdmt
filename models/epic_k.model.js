module.exports = (sequelize, Sequelize) => {
    const EpicK = sequelize.define("epicK", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        epicId: {
            type: Sequelize.INTEGER,
        },
        coefficient: {
            type: Sequelize.DOUBLE,
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate : Sequelize.NOW,
        },
    },
        {
            indexes: [
                {
                    unique: true,
                    fields: ['epicId', 'coefficient']
                }
            ]
        })

    return EpicK
}