module.exports = (sequelize, Sequelize) => {
    const EpicTeam = sequelize.define("epicTeam", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        epicId: {
            type: Sequelize.INTEGER,
        },
        teamId: {
            type: Sequelize.INTEGER,
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate : Sequelize.NOW,
        },
    },
        {
            indexes: [
                {
                    unique: true,
                    fields: ['epicId', 'teamId']
                }
            ]
        })

    return EpicTeam
}