const config = require('config')
const Sequelize = require('sequelize')

const dbConfig = config.get('DBConn')

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    // operatorsAliases: false,
    logging: dbConfig.logging,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    },
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

// Company
db.company = require('./company.model')(sequelize, Sequelize)

// USERS
db.user = require('./user.model')(sequelize, Sequelize)
db.role = require('./role.model')(sequelize, Sequelize)
db.role_user = require('./role_user.model')(sequelize, Sequelize)
// role users reference, on delete user -> deleting role_user
db.user.belongsToMany(db.role, { through: db.role_user, onDelete: 'CASCADE' })
db.role.belongsToMany(db.user, { through: db.role_user, onDelete: 'CASCADE' })
//
db.user.belongsTo(db.company, { foreignKey: 'companyId' })
db.company.hasMany(db.user, { foreignKey: 'companyId' })

// PROJECTS
db.project = require('./project.model')(sequelize, Sequelize)
db.project_user = require('./project_user.model')(sequelize, Sequelize)

db.project.belongsToMany(db.user, { through: db.project_user, onDelete: 'CASCADE' })
db.user.belongsToMany(db.project, { through: db.project_user, onDelete: 'CASCADE' })

db.project.belongsTo(db.company, { foreignKey: 'companyId' })
db.company.hasMany(db.project, { foreignKey: 'companyId' })

// BLOCKS
db.block = require('./block.model')(sequelize, Sequelize)

db.project.hasMany(db.block, { foreignKey: 'projectId' })
db.block.belongsTo(db.project, { foreignKey: 'projectId', onDelete: 'CASCADE' })

db.property = require('./property.model')(sequelize, Sequelize)


db.block.hasMany(db.property, { foreignKey: 'blockId' })
db.property.belongsTo(db.block, { foreignKey: 'blockId', onDelete: 'CASCADE' })


db.moduleSkill = require('./moduleSkill.model')(sequelize, Sequelize)

db.block.hasMany(db.moduleSkill, { foreignKey: 'blockId' })
db.moduleSkill.belongsTo(db.block, { foreignKey: 'blockId', onDelete: 'CASCADE' })


db.modulePeriod = require('./modulePeriod.model')(sequelize, Sequelize)

db.block.hasMany(db.modulePeriod, { foreignKey: 'blockId' })
db.modulePeriod.belongsTo(db.block, { foreignKey: 'blockId', onDelete: 'CASCADE' })



db.option = require('./option.model')(sequelize, Sequelize)

db.property.hasMany(db.option, { foreignKey: 'propertyId' })
db.option.belongsTo(db.property, { foreignKey: 'propertyId', onDelete: 'CASCADE' })

// SKILL
db.skill = require('./skill.model')(sequelize, Sequelize)

db.project.hasMany(db.skill, { foreignKey: 'projectId' })
db.skill.belongsTo(db.project, { foreignKey: 'projectId', onDelete: 'CASCADE' })

// TEAM
db.team = require('./team.model')(sequelize, Sequelize)
db.teamUSkill = require('./team_userSkill.model')(sequelize, Sequelize)
db.epicTeam = require('./epic_team.model')(sequelize, Sequelize)
db.epicK = require('./epic_k.model')(sequelize, Sequelize)

db.team.hasMany(db.teamUSkill, { foreignKey: 'teamId', onDelete: 'CASCADE' })
db.teamUSkill.belongsTo(db.team, { foreignKey: 'teamId', onDelete: 'CASCADE' })

db.project.hasMany(db.team, { foreignKey: 'projectId' })
db.team.belongsTo(db.project, { foreignKey: 'projectId', onDelete: 'CASCADE' })

db.user.hasMany(db.team, { foreignKey: 'userId' })
db.team.belongsTo(db.user, { foreignKey: 'userId', onDelete: 'CASCADE' })

db.teamUSkill.belongsTo(db.skill, { foreignKey: 'skillId' })
// db.team.belongsTo(db.skill, {foreignKey: 'skillId', onDelete: 'CASCADE' })

db.team.hasMany(db.epicTeam, { foreignKey: 'teamId', onDelete: 'CASCADE' })
db.epicTeam.belongsTo(db.team, { foreignKey: 'teamId', onDelete: 'CASCADE' })

module.exports = db