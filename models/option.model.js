module.exports = (sequelize, Sequelize) => {
    const Option = sequelize.define("option", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        propertyId: {
            type: Sequelize.INTEGER,
        },
        text: {
            type: Sequelize.STRING
        },
        file: {
            type: Sequelize.STRING
        },
        path: {
            type: Sequelize.STRING
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate : Sequelize.NOW,
        },
    })

    return Option
}