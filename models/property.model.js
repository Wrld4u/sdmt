module.exports = (sequelize, Sequelize) => {
    const Property = sequelize.define("property", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        blockId: {
            type: Sequelize.INTEGER,
        },
        name: {
            type: Sequelize.STRING
        },
        type: {
            type: Sequelize.STRING
        },
        selectedId: {
            type: Sequelize.INTEGER,
            defaultValue: 0,
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate : Sequelize.NOW,
        },
    })

    return Property
}