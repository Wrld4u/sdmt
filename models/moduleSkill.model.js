module.exports = (sequelize, Sequelize) => {
    const ModuleSkill = sequelize.define("module_skill", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        blockId: {
            type: Sequelize.INTEGER,
        },
        skill: {
            type: Sequelize.STRING
        },
        level: {
            type: Sequelize.INTEGER
        },
        amount: {
            type: Sequelize.INTEGER
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate: Sequelize.NOW,
        },
    })

    return ModuleSkill
}